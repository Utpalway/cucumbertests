package com.classes.OnlineSelenium.Cucumber;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Dashboard 
{
	
	@Given("{string}, {string} link should be persent on the dashboard page")
	public void link_should_be_persent_on_the_dashboard_page(String string, String string2) {
	    // Write code here that turns the phrase above into concrete actions
	    System.out.println("This is from Given");
	}
	
	@When("Tutor click on notifications link from dahsboard")
	public void tutor_click_on_notifications_link_from_dahsboard() {
	    // Write code here that turns the phrase above into concrete actions
	    System.out.println("When notification page");
	}

	@Then("Tutor should see the notifications Page")
	public void tutor_should_see_the_notifications_page() {
	    // Write code here that turns the phrase above into concrete actions
		System.out.println("Then notification page");
	}

	@When("Tutor click on chats link from dahsboard")
	public void tutor_click_on_chats_link_from_dahsboard() {
	    // Write code here that turns the phrase above into concrete actions
		System.out.println("When chats page");
	}

	@Then("Tutor should see the chats Page")
	public void tutor_should_see_the_chats_page() {
	    // Write code here that turns the phrase above into concrete actions
		System.out.println("Then chats page");
	}

	@When("Tutor click on my earnings link from dahsboard")
	public void tutor_click_on_my_earnings_link_from_dahsboard() {
	    // Write code here that turns the phrase above into concrete actions
		System.out.println("When my earnings page");
	}

	@Then("Tutor should see the my earnings Page")
	public void tutor_should_see_the_my_earnings_page() {
	    // Write code here that turns the phrase above into concrete actions
		System.out.println("Then my earnings page");
	}
	
	@When("Tutor click on Tutoring Preferences link from dahsboard")
	public void tutor_click_on_tutoring_preferences_link_from_dahsboard() 
	{
	    // Write code here that turns the phrase above into concrete actions
		System.out.println("When Tutoring Preferences page");
	}

	@Then("Tutor should see the Tutoring Preferences Page")
	public void tutor_should_see_the_tutoring_preferences_page() 
	{
	    // Write code here that turns the phrase above into concrete actions
		System.out.println("Then Tutoring Preferences page");
	}

	@Then("Page title should contains with subjects")
	public void page_title_should_contains_with_subjects() 
	{
	    // Write code here that turns the phrase above into concrete actions
		System.out.println("Common for all Page title should contains with subjects");
	}

	@When("Tutor click on my education link from dashboard page")
	public void tutor_click_on_my_education_link_from_dashboard_page() 
	{
	    // Write code here that turns the phrase above into concrete actions
		System.out.println("When my education page");
	}

	@Then("Tutor should see the my education page")
	public void tutor_should_see_the_my_education_page()
	{
	    // Write code here that turns the phrase above into concrete actions
		System.out.println("Then my education page");
	}

	@Then("The page title should contains with education")
	public void the_page_title_should_contains_with_education()
	{
	    // Write code here that turns the phrase above into concrete actions
		System.out.println("Then:--The page title should contains with education");
	}

	@When("Tutor click on teaching subjects link from dashboard page")
	public void tutor_click_on_teaching_subjects_link_from_dashboard_page() 
	{
	    // Write code here that turns the phrase above into concrete actions
		System.out.println("When teaching subjects Page");
	}

	@Then("Tutor should see the teaching subjects page")
	public void tutor_should_see_the_teaching_subjects_page() 
	{
	    // Write code here that turns the phrase above into concrete actions
		System.out.println("Then teaching subjects Page");
	}

	@Then("The page title should contains with subjects")
	public void the_page_title_should_contains_with_subjects() 
	{
	    // Write code here that turns the phrase above into concrete actions
		System.out.println("Then teaching subjects Page");
	}

	@When("Tutor click on slots of booking link from dashboard page")
	public void tutor_click_on_slots_of_booking_link_from_dashboard_page() 
	{
	    // Write code here that turns the phrase above into concrete actions
		System.out.println("When slots of booking Page");
	}

	@Then("Tutor should see the slots of booking page")
	public void tutor_should_see_the_slots_of_booking_page() 
	{
	    // Write code here that turns the phrase above into concrete actions
		System.out.println("Then slots of booking Page");
	}

	@Then("The page title should contains with slots")
	public void the_page_title_should_contains_with_slots() 
	{
	    // Write code here that turns the phrase above into concrete actions
		System.out.println("Then slots of booking Page");
	}

	@When("Tutor click on the My Blogs Link from dashboard page")
	public void tutor_click_on_the_my_blogs_link_from_dashboard_page()
	{
	    // Write code here that turns the phrase above into concrete actions
		System.out.println("When My Blogs Page");
	}

	@Then("Tutor should be able to see My Blogs page")
	public void tutor_should_be_able_to_see_my_blogs_page()
	{
	    // Write code here that turns the phrase above into concrete actions
		System.out.println("Then My Blogs Page");
	}

	@Then("Page title should contains with blogs")
	public void page_title_should_contains_with_blogs()
	{
	    // Write code here that turns the phrase above into concrete actions
		System.out.println("Then My Blogs Page");
	}

	@When("Tutor click on the Change Password link from dashboard page")
	public void tutor_click_on_the_change_password_link_from_dashboard_page() 
	{
	    // Write code here that turns the phrase above into concrete actions
		System.out.println("When Change Password Page");
	}

	@Then("Tutor should be able to see Password Change page")
	public void tutor_should_be_able_to_see_password_change_page() 
	{
	    // Write code here that turns the phrase above into concrete actions
		System.out.println("Then Change Password Page");
	}

	@Then("Page title should contains with change")
	public void page_title_should_contains_with_change() 
	{
	    // Write code here that turns the phrase above into concrete actions
		System.out.println("Then Change Password Page");
	}

	@When("Tutor click on Bank Details link from dashboard page")
	public void tutor_click_on_bank_details_link_from_dashboard_page() 
	{
	    // Write code here that turns the phrase above into concrete actions
		System.out.println("When Bank Details Page");
	}

	@Then("Tutor should be able to see Bank Details page")
	public void tutor_should_be_able_to_see_bank_details_page() 
	{
	    // Write code here that turns the phrase above into concrete actions
		System.out.println("Then Bank Details Page");
	}

	@Then("Page title should contains with payment")
	public void page_title_should_contains_with_payment()
	{
	    // Write code here that turns the phrase above into concrete actions
		System.out.println("Then Bank Details Page");
	}

	@When("Tutor click on FAQs link from dashboard page")
	public void tutor_click_on_fa_qs_link_from_dashboard_page() 
	{
	    // Write code here that turns the phrase above into concrete actions
		System.out.println("When FAQs Page");
	}

	@Then("Tutor should be able to see FAQs page")
	public void tutor_should_be_able_to_see_fa_qs_page() 
	{
	    // Write code here that turns the phrase above into concrete actions
		System.out.println("Then FAQs Page");
	}

	@Then("Page title should contains with faq")
	public void page_title_should_contains_with_faq() 
	{
	    // Write code here that turns the phrase above into concrete actions
		System.out.println("Then FAQs Page");
	}

}
