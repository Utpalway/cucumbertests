Feature: Tutor should be able to navigate different pages from dahsboard

  Background: Tutor at the dashboard page

  @SmokeTest
  Scenario Outline: Tutor should be able to view notifications page
    Given "<TabValue>", "<Link>" link should be persent on the dashboard page
    When Tutor click on notifications link from dahsboard
    Then Tutor should see the notifications Page
    And Page title should contains with subjects

    Examples: 
      | Link                                   | TabValue      |
      | https://skiedo.com/tutor/notifications | Notifications |

  @SmokeTest
  Scenario Outline: Tutor should be able to view chats page
    Given "<TabValue>", "<Link>" link should be persent on the dashboard page
    When Tutor click on chats link from dahsboard
    Then Tutor should see the chats Page
    And Page title should contains with subjects

    Examples: 
      | Link                            | TabValue |
      | https://skiedo.com/tutor/chatPN | Chats    |

  @SmokeTest
  Scenario Outline: Tutor should be able to view my earnings page
    Given "<TabValue>", "<Link>" link should be persent on the dashboard page
    When Tutor click on my earnings link from dahsboard
    Then Tutor should see the my earnings Page
    And Page title should contains with subjects

    Examples: 
      | Link                                        | TabValue    |
      | https://skiedo.com/tutor/payment/statistics | my earnings |

  @SmokeTest
  Scenario Outline: Tutor should be able to view my basic information page
    Given "<TabValue>", "<Link>" link should be persent on the dashboard page
    When Tutor click on my earnings link from dahsboard
    Then Tutor should see the my earnings Page
    And Page title should contains with subjects

    Examples: 
      | Link                             | TabValue             |
      | https://skiedo.com/tutor/profile | my basic information |

  @SmokeTest
  Scenario Outline: Tutor should be able to view tutoring preferences page
    Given "<TabValue>", "<Link>" link should be persent on the dashboard page
    When Tutor click on my earnings link from dahsboard
    Then Tutor should see the my earnings Page
    And Page title should contains with subjects

    Examples: 
      | Link                                 | TabValue             |
      | https://skiedo.com/tutor/tutorprefer | tutoring preferences |

  @SmokeTest
  Scenario Outline: Tutor should be able to view my education page
    Given "<TabValue>", "<Link>" link should be persent on the dashboard page
    When Tutor click on my education link from dashboard page
    Then Tutor should see the my education page
    And The page title should contains with education

    Examples: 
      | Link                               | TabValue     |
      | https://skiedo.com/tutor/education | my education |

  @SmokeTest
  Scenario Outline: Tutor should be able to view teaching subjects page
    Given "<TabValue>", "<Link>" link should be persent on the dashboard page
    When Tutor click on teaching subjects link from dashboard page
    Then Tutor should see the teaching subjects page
    And The page title should contains with subjects

    Examples: 
      | Link                              | TabValue          |
      | https://skiedo.com/tutor/subjects | teaching subjects |

  @SmokeTest
  Scenario Outline: Tutor should be able to view slots of booking page
    Given "<TabValue>", "<Link>" link should be persent on the dashboard page
    When Tutor click on slots of booking link from dashboard page
    Then Tutor should see the slots of booking page
    And The page title should contains with slots

    Examples: 
      | Link                           | TabValue         |
      | https://skiedo.com/tutor/slots | slots of booking |

  @SmokeTest
  Scenario Outline: Tutor should be able to view My Blogs page
    Given "<TabValue>", "<Link>" link should be persent on the dashboard page
    When Tutor click on the My Blogs Link from dashboard page
    Then Tutor should be able to see My Blogs page
    And Page title should contains with blogs

    Examples: 
      | Link                           | TabValue |
      | https://skiedo.com/tutor/blogs | My Blogs |

  @SmokeTest
  Scenario Outline: Tutor should be able to view Change Password page
    Given "<TabValue>", "<Link>" link should be persent on the dashboard page
    When Tutor click on the Change Password link from dashboard page
    Then Tutor should be able to see Password Change page
    And Page title should contains with change

    Examples: 
      | Link                                     | TabValue        |
      | https://skiedo.com/tutor/password/change | Change Password |

  @SmokeTest
  Scenario Outline: Tutor should be able to view Bank Details page
    Given "<TabValue>", "<Link>" link should be persent on the dashboard page
    When Tutor click on Bank Details link from dashboard page
    Then Tutor should be able to see Bank Details page
    And Page title should contains with payment

    Examples: 
      | Link                             | TabValue     |
      | https://skiedo.com/tutor/payment | Bank Details |

  @SmokeTest
  Scenario Outline: Tutor should be able to view FAQs page
    Given "<TabValue>", "<Link>" link should be persent on the dashboard page
    When Tutor click on FAQs link from dashboard page
    Then Tutor should be able to see FAQs page
    And Page title should contains with faq

    Examples: 
      | Link                         | TabValue |
      | https://skiedo.com/tutor/faq | FAQs     |
